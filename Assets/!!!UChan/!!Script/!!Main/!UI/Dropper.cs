﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Dropper : MonoBehaviour , IDropHandler
{
    void IDropHandler.OnDrop(PointerEventData eventData)
    {
        GameObject graphic = eventData.pointerDrag;
        if(graphic != null)
        {
            graphic.transform.SetParent( this.transform);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
