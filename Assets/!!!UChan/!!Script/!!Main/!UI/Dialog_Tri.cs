﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
public class Dialog_Tri : MonoBehaviour
{
    [Header("區域一的凱爾對話01")]
    public bool d_tri_01;
    [Header("區域一的凱爾對話02")]
    public bool d_tri_02;
    public UIManager uIManager;
    void Start()
    {
        uIManager = GameObject.Find("UIManager").GetComponent<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == ("Controller"))
        {
            if(d_tri_01 == true)
            {
                print("進入第一階段對話");
                uIManager.StartCoroutine(uIManager.Type());
                d_tri_01 = false;
                
               
            }
            if (d_tri_02 == true)
            {
                uIManager.StartCoroutine(uIManager.Type_02());
                print("進入第二階段對話");
                d_tri_02 = false;
            }
        }
       
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == ("Controller"))
        {
            uIManager.isDialog = false;
            
        }
    }
}
