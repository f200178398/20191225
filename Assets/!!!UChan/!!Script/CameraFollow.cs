﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    protected Transform XfromCamera;
    protected Transform XfromParent;

    protected Vector3 LocalRotationOfCamera;
    protected float CameraDistance = 10f;





    // Use this for initialization
    void Start()
    {
        this.XfromCamera = this.transform;
        this.XfromParent = this.transform.parent;
    }

    // Update is called once per frame
    void Update()
    {

    }


    public float MouseSensetive = 2.0f;
    public float ScrollSensetive = 1.0f;
    public float OrbitDampening = 10f;  //畫面停止轉動時的平滑度
    public float ScrollDampening = 6f; //滾輪拉近拉遠畫面時的平滑度


    public bool CameraDisable = false;



    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CameraDisable = !CameraDisable;//camera=ture
        }
        if (!CameraDisable)
        {
            //相機跟著滑鼠移動轉動鏡頭
            if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
            {
                LocalRotationOfCamera.x += Input.GetAxis("Mouse X") * MouseSensetive;
                LocalRotationOfCamera.y -= Input.GetAxis("Mouse Y") * MouseSensetive;

                //相機上下角度轉不超過90度，反則脖子會扭到等等，當轉角<0或>90時，強制等於0~90
                //if (LocalRotationOfCamera.y < 0f)
                //{
                //    LocalRotationOfCamera.y = 0f;
                //}
                //else if (LocalRotationOfCamera.y > 90f)
                //{
                //    LocalRotationOfCamera.y = 90f;
                //}
                LocalRotationOfCamera.y = Mathf.Clamp(LocalRotationOfCamera.y, 0f, 90f);//這一行就是上面那一串
            }

            //下面這一串控制Zoom in ,Zomm out
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                Debug.Log("Mouse ScrollWheel" + Input.GetAxis("Mouse ScrollWheel"));
                float ScrollAmount = Input.GetAxis("Mouse ScrollWheel") * ScrollSensetive;
                //
                ScrollAmount *= (this.CameraDistance * 0.3f);//
                //

                this.CameraDistance += ScrollAmount * -1.0f;
                //

                //限制相機 與 軸心(主目標.主角)距離
                this.CameraDistance = Mathf.Clamp(this.CameraDistance, 1.5f, 20f);
            }
        }
        Quaternion QT = Quaternion.Euler(LocalRotationOfCamera.y, LocalRotationOfCamera.x, 0f);
        this.XfromParent.rotation = Quaternion.Lerp(this.XfromParent.rotation, QT, Time.deltaTime * OrbitDampening); //畫面停止轉動後的平滑感

        if (this.XfromCamera.localPosition.z != this.CameraDistance * -1f)
        {
            this.XfromCamera.localPosition = new Vector3(0f, 0f, Mathf.Lerp(this.XfromCamera.localPosition.z, this.CameraDistance * -1f, Time.deltaTime * ScrollDampening));
        }





    }
}
