﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RAYtest : MonoBehaviour
{
    public RaycastHit targetHit;
    public Ray controllerRay;
    public float rayDistance;
    
    public Collider Enemy;

    void Start()
    {
        controllerRay = new Ray(transform.position, transform.forward); //射線初始位置 和 發射方向 定義
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(controllerRay);
        if (Physics.Raycast(controllerRay,out targetHit, rayDistance))
        {
            if(targetHit.collider.tag == "Enemy")
            {
                Debug.Log("Enemy");
                Debug.Log(targetHit.collider);
                Enemy = targetHit.collider;
                //Destroy(Enemy);
            }
        }

    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, new Vector3(0, 5, 0));
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, Vector3.forward);
    }
}
