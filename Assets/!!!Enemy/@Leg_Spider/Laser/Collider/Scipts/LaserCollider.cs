﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bubble;

public class LaserCollider : MonoBehaviour
{

    public GameObject spiderRobotTurretLaserEmitter;
    public InvisibleBubblePool invisibleBubblePool;
    public Collider laserClo;
    // Start is called before the first frame update
    void Start()
    {
        laserClo = gameObject.transform.GetChild(0).gameObject.GetComponent<Collider>();
        invisibleBubblePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<InvisibleBubblePool>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //PositionSetting();
        LaserColliderLifeTime();
    }

    private void OnEnable()
    {
        //spiderRobotTurretLaserEmitter = GameObject.FindGameObjectWithTag("LaserEmitter");
        _timer = Time.time;
    }

    void PositionSetting()
    {
        this.transform.position = spiderRobotTurretLaserEmitter.transform.position;
        this.transform.rotation = spiderRobotTurretLaserEmitter.transform.rotation;
    }


    [Header("圓柱體的生命")]
    public float _timer;
    public float bubbleLife = 10.0f;

    void LaserColliderLifeTime()
    {
        //下面這段if，應該只是防呆，有測試過如果刪掉還是一樣可以執行
        if (!gameObject.activeInHierarchy)
            return;

        if (Time.time > _timer + bubbleLife)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            invisibleBubblePool.Recovery(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == ("Controller"))
        {
            laserClo.enabled = !enabled;
            invisibleBubblePool.Recovery(this.gameObject);
        }
    }






}
